﻿using ITL.Asset.Models;
using JAFF.Helper.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ITL.Asset.DAL
{
    public class AssetContext : DbContext
    {
        public DbSet<tbl_item> tbl_item { get; set; }
        public DbSet<tbl_ref_jenis_katalog> tbl_ref_jenis_katalog { get; set; }
        public DbSet<tbl_katalog> tbl_katalog { get; set; }
        public DbSet<tbl_ref_lokasi> tbl_ref_lokasi { get; set; }
        public virtual DbSet<SysLog> SysLogs { get; set; }

        private static string Connection()
        {
            var conn = ConfigurationManager.ConnectionStrings["AST"].ConnectionString;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(conn);
            string user = builder.UserID;
            string pass = builder.Password;
            builder.Password = EncryptorWrapper.DecryptCs(pass);
            var newcon = builder.ToString();
            return newcon;
        }

        public AssetContext()
            : base(Connection())
        {
            //Configuration.ProxyCreationEnabled = false;
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 280;
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<AssetContext>(null);
            base.OnModelCreating(modelBuilder);
        }

        #region Audit Log
        public int SaveChanges(string userId)
        {
            try
            {
                // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
                foreach (var ent in this.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
                {
                    // For each changed record, get the audit record entries and add them
                    foreach (SysLog x in GetAuditRecordsForChange(ent, userId))
                    {
                        this.SysLogs.Add(x);
                    }
                }
            }
            catch
            {
            }

            // Call the original SaveChanges(), which will save both the changes made and the audit records
            return base.SaveChanges();
        }

        private string GetPrimaryKeyName(DbEntityEntry entry)
        {
            var objectStateEntry = ((IObjectContextAdapter)this).ObjectContext.ObjectStateManager.GetObjectStateEntry(entry.Entity);
            return objectStateEntry.EntitySet.ElementType.KeyMembers.Single().Name;
        }

        private List<SysLog> GetAuditRecordsForChange(DbEntityEntry dbEntry, string userId)
        {
            List<SysLog> result = new List<SysLog>();

            DateTime changeTime = DateTime.Now;
            TableAttribute tableAttr = null;
            string tableName = string.Empty;
            string keyName = string.Empty;

            try
            {
                // Get the Table() attribute, if one exists
                tableAttr = dbEntry.Entity.GetType().GetCustomAttributes(typeof(TableAttribute), false).SingleOrDefault() as TableAttribute;

                // Get table name (if it has a Table attribute, use that, otherwise get the pluralized name)
                tableName = tableAttr != null ? tableAttr.Name : dbEntry.Entity.GetType().Name;

                // Get primary key value (If you have more than one key column, this will need to be adjusted)
                keyName = GetPrimaryKeyName(dbEntry);


                if (dbEntry.State == EntityState.Added)
                {
                    // For Inserts, just add the whole record
                    result.Add(new SysLog()
                    {
                        UserID = userId,
                        EventDateUTC = changeTime,
                        EventType = "A", // Added
                        TableName = tableName,
                        RecordID = dbEntry.CurrentValues.GetValue<object>(keyName).ToString(),
                        ColumnName = "*ALL",
                        CurrentValue = DescribeEntity(dbEntry.CurrentValues.ToObject())
                    }
                        );
                }
                else if (dbEntry.State == EntityState.Deleted)
                {
                    // Same with deletes, do the whole record
                    result.Add(new SysLog()
                    {
                        UserID = userId,
                        EventDateUTC = changeTime,
                        EventType = "D", // Deleted
                        TableName = tableName,
                        RecordID = dbEntry.OriginalValues.GetValue<object>(keyName).ToString(),
                        ColumnName = "*ALL",
                        BeforeValue = DescribeEntity(dbEntry.OriginalValues.ToObject())
                    }
                        );
                }
                else if (dbEntry.State == EntityState.Modified)
                {
                    foreach (string propertyName in dbEntry.OriginalValues.PropertyNames)
                    {
                        // For updates, we only want to capture the columns that actually changed
                        if (!object.Equals(dbEntry.GetDatabaseValues().GetValue<object>(propertyName), dbEntry.CurrentValues.GetValue<object>(propertyName)))
                        {
                            result.Add(new SysLog()
                            {
                                UserID = userId,
                                EventDateUTC = changeTime,
                                EventType = "M",    // Modified
                                TableName = tableName,
                                RecordID = dbEntry.OriginalValues.GetValue<object>(keyName).ToString(),
                                ColumnName = propertyName,
                                BeforeValue = dbEntry.GetDatabaseValues().GetValue<object>(propertyName) == null ? null : dbEntry.GetDatabaseValues().GetValue<object>(propertyName).ToString(),
                                CurrentValue = dbEntry.CurrentValues.GetValue<object>(propertyName) == null ? null : dbEntry.CurrentValues.GetValue<object>(propertyName).ToString()
                            }
                                );
                        }
                    }
                }
            }
            catch
            {
            }

            return result;
        }

        private string DescribeEntity(object obj)
        {
            string returnValue = string.Empty;
            returnValue = "{";
            var properties = from p in obj.GetType().GetProperties()
                             where p.CanRead && p.CanWrite
                             select p;
            foreach (var property in properties)
            {
                var value = property.GetValue(obj, null) == null ? string.Empty : property.GetValue(obj, null).ToString();
                returnValue += property.Name + ":" + "\"" + value + "\" , ";

            }
            returnValue += "}";
            return returnValue;
        }
        #endregion
    }
}