﻿using ITL.Asset.DAL;
using JAFF.Helper.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ITL.Asset.Controllers
{
    public class BaseController : Controller
    {
        protected AssetContext db = new AssetContext();

        protected ResultMessage resultMessage = new ResultMessage();
        protected DataTable dt = new DataTable();
	}
}