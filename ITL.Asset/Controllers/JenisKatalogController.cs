﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITL.Asset.Models;

namespace ITL.Asset.Controllers
{
    public class JenisKatalogController : BaseController
    {
        // GET: JenisKatalog
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int draw, int start, int length)
        {
            try
            {
                string search = Request["search[value]"];

                var data = db.tbl_ref_jenis_katalog.Where(d => d.deleted_on == null).AsQueryable();

                dt.recordsTotal = data.Count();

                if (!String.IsNullOrEmpty(search))
                {
                    data = data.Where(d => d.name.Contains(search));
                }

                var getdata = data.OrderBy(d => d.name).Skip(start).Take(length).ToList();

                dt.recordsFiltered = data.Count();

                dt.data = getdata;
                dt.draw = draw;
                return Json(dt);
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;

                return Json(resultMessage);
            }
        }

        public ActionResult tambah_jenis_katalog()
        {
            return View();
        }

        public ActionResult simpan_jenis_katalog()
        {
            try
            {
                tbl_ref_jenis_katalog jv = new tbl_ref_jenis_katalog();
                jv.id = Guid.NewGuid();
                jv.name = Request["name"];
                jv.created_by = "System Check";
                jv.created_on = DateTime.Now;

                db.tbl_ref_jenis_katalog.Add(jv);
                db.SaveChanges();
                resultMessage.message = "Sukses";
                resultMessage.code = 1;
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;
            }

            return Json(resultMessage);
        }

        public ActionResult hapus_jenis_katalog(Guid id)
        {
            try
            {
                var data_jenis_katalog = db.tbl_ref_jenis_katalog.Where(d => d.id == id).FirstOrDefault();
                data_jenis_katalog.deleted_by = "System Check";
                data_jenis_katalog.deleted_on = DateTime.Now;

                db.SaveChanges();
                resultMessage.message = "Sukses";
                resultMessage.code = 1;
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;
            }

            return Json(resultMessage);
        }

        public ActionResult edit_jenis_katalog(Guid id)
        {
            ViewBag.data_jenis_katalog = db.tbl_ref_jenis_katalog.Where(d => d.id == id).FirstOrDefault();

            return View();
        }

        public ActionResult update_jenis_katalog(Guid id)
        {
            try
            {
                var data = db.tbl_ref_jenis_katalog.Where(d => d.id == id).FirstOrDefault();
                data.name = Request["name"];
                data.modified_by = "System Check";
                data.modified_on = DateTime.Now;

                db.SaveChanges();
                resultMessage.message = "Sukses";
                resultMessage.code = 1;
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;
            }

            return Json(resultMessage);
        }
    }
}