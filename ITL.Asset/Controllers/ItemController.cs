﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITL.Asset.Models;

namespace ITL.Asset.Controllers
{
    public class ItemController : BaseController
    {
        // GET: Item
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int draw, int start, int length)
        {
            try
            {
                string search = Request["search[value]"];

                var data = db.tbl_item.Where(d => d.deleted_on == null).AsQueryable();

                dt.recordsTotal = data.Count();

                if (!String.IsNullOrEmpty(search))
                {
                    data = data.Where(d => d.name.Contains(search));
                }

                var getdata = data.OrderBy(d => d.name).Skip(start).Take(length).ToList();

                dt.recordsFiltered = data.Count();

                dt.data = getdata;
                dt.draw = draw;
                return Json(dt);
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;

                return Json(resultMessage);
            }
        }

        public ActionResult tambah_item()
        {
            ViewBag.ref_lokasi = db.tbl_ref_lokasi.Where(d => d.deleted_on == null).ToList();
            ViewBag.ref_katalog = db.tbl_katalog.Where(d => d.deleted_on == null).ToList();
            return View();
        }

        public ActionResult simpan_item()
        {
            try
            {
                tbl_item jv = new tbl_item();
                jv.id = Guid.NewGuid();
                jv.name = Request["name"];
                jv.id_lokasi = Guid.Parse(Request["id_lokasi"]);
                jv.id_katalog = Guid.Parse(Request["id_katalog"]);
                jv.no_inventaris = Request["no_inventaris"];
                jv.kondisi = Request["kondisi"];
                jv.no_ajuan = Convert.ToInt32(Request["no_ajuan"]);
                jv.harga = Convert.ToInt64(Request["harga"]);
                jv.garansi = Convert.ToInt32(Request["garansi"]);
                jv.created_by = "System Check";
                jv.created_on = DateTime.Now;

                db.tbl_item.Add(jv);
                db.SaveChanges();
                resultMessage.message = "Sukses";
                resultMessage.code = 1;
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;
            }

            return Json(resultMessage);
        }

        public ActionResult hapus_item(Guid id)
        {
            try
            {
                var data_lokasi = db.tbl_item.Where(d => d.id == id).FirstOrDefault();
                data_lokasi.deleted_by = "System Check";
                data_lokasi.deleted_on = DateTime.Now;

                db.SaveChanges();
                resultMessage.message = "Sukses";
                resultMessage.code = 1;
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;
            }

            return Json(resultMessage);
        }

        public ActionResult edit_item(Guid id)
        {
            ViewBag.ref_lokasi = db.tbl_ref_lokasi.Where(d => d.deleted_on == null).ToList();
            ViewBag.ref_katalog = db.tbl_katalog.Where(d => d.deleted_on == null).ToList();
            ViewBag.data_item = db.tbl_item.Where(d => d.id == id).FirstOrDefault();

            return View();
        }

        public ActionResult update_item(Guid id)
        {
            try
            {
                var data = db.tbl_item.Where(d => d.id == id).FirstOrDefault();
                data.name = Request["name"];
                data.id_lokasi = Guid.Parse(Request["id_lokasi"]);
                data.id_katalog = Guid.Parse(Request["id_katalog"]);
                data.no_inventaris = Request["no_inventaris"];
                data.kondisi = Request["kondisi"];
                data.no_ajuan = Convert.ToInt32(Request["no_ajuan"]);
                data.harga = Convert.ToInt64(Request["harga"]);
                data.modified_by = "System Check";
                data.modified_on = DateTime.Now;

                db.SaveChanges();
                resultMessage.message = "Sukses";
                resultMessage.code = 1;
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;
            }

            return Json(resultMessage);
        }
    }
}