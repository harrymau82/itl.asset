﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITL.Asset.Models;

namespace ITL.Asset.Controllers
{
    public class LokasiController : BaseController
    {
        //
        // GET: /Lokasi/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int draw, int start, int length)
        {
            try
            {
                string search = Request["search[value]"];

                var data = db.tbl_ref_lokasi.Where(d => d.deleted_on == null).AsQueryable();

                dt.recordsTotal = data.Count();

                if (!String.IsNullOrEmpty(search))
                {
                    data = data.Where(d => d.name.Contains(search) || d.kd_lokasi.Contains(search));
                }

                var getdata = data.OrderBy(d => d.name).Skip(start).Take(length).ToList();

                dt.recordsFiltered = data.Count();

                dt.data = getdata;
                dt.draw = draw;
                return Json(dt);
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;

                return Json(resultMessage);
            }
        }

        public ActionResult tambah_lokasi() 
        {
            return View();
        }

        public ActionResult simpan_lokasi()
        {
            try
            {
                tbl_ref_lokasi jv = new tbl_ref_lokasi();
                jv.id = Guid.NewGuid();
                jv.name = Request["name"];
                jv.kd_lokasi = Request["kd_lokasi"];
                jv.created_by = "System Check";
                jv.created_on = DateTime.Now;

                db.tbl_ref_lokasi.Add(jv);
                db.SaveChanges();
                resultMessage.message = "Sukses";
                resultMessage.code = 1;
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;
            }

            return Json(resultMessage);
        }

        public ActionResult hapus_lokasi(Guid id)
        {
            try
            {
                var data_lokasi = db.tbl_ref_lokasi.Where(d => d.id == id).FirstOrDefault();
                data_lokasi.deleted_by = "System Check";
                data_lokasi.deleted_on = DateTime.Now;

                db.SaveChanges();
                resultMessage.message = "Sukses";
                resultMessage.code = 1;
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;
            }

            return Json(resultMessage);
        }

        public ActionResult edit_lokasi(Guid id)
        {
            ViewBag.data_lokasi = db.tbl_ref_lokasi.Where(d => d.id == id).FirstOrDefault();

            return View();
        }

        public ActionResult update_lokasi(Guid id)
        {
            try
            {
                var data_lokasi = db.tbl_ref_lokasi.Where(d => d.id == id).FirstOrDefault();
                data_lokasi.name = Request["name"];
                data_lokasi.kd_lokasi = Request["kd_lokasi"];
                data_lokasi.modified_by = "System Check";
                data_lokasi.modified_on = DateTime.Now;

                db.SaveChanges();
                resultMessage.message = "Sukses";
                resultMessage.code = 1;
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;
            }

            return Json(resultMessage);
        }
    }
}