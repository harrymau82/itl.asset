﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ITL.Asset.Models;

namespace ITL.Asset.Controllers
{
    public class KatalogController : BaseController
    {
        // GET: Katalog
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int draw, int start, int length)
        {
            try
            {
                string search = Request["search[value]"];

                var data = db.tbl_katalog.Where(d => d.deleted_on == null).AsQueryable();

                dt.recordsTotal = data.Count();

                if (!String.IsNullOrEmpty(search))
                {
                    data = data.Where(d => d.name.Contains(search));
                }

                var getdata = data.OrderBy(d => d.name).Skip(start).Take(length).ToList();

                dt.recordsFiltered = data.Count();

                dt.data = getdata;
                dt.draw = draw;
                return Json(dt);
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;

                return Json(resultMessage);
            }
        }

        public ActionResult tambah_katalog()
        {
            ViewBag.ref_jenis_katalog = db.tbl_ref_jenis_katalog.Where(d => d.deleted_by == null).ToList();

            return View();
        }

        public ActionResult simpan_katalog()
        {
            try
            {
                tbl_katalog jv = new tbl_katalog();
                jv.id = Guid.NewGuid();
                jv.name = Request["name"];
                jv.harga = Convert.ToInt64(Request["harga"]);
                jv.id_jenis_katalog = Guid.Parse(Request["id_jenis_katalog"]);
                jv.created_by = "System Check";
                jv.created_on = DateTime.Now;

                db.tbl_katalog.Add(jv);
                db.SaveChanges();
                resultMessage.message = "Sukses";
                resultMessage.code = 1;
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;
            }

            return Json(resultMessage);
        }

        public ActionResult hapus_katalog(Guid id)
        {
            try
            {
                var data_katalog = db.tbl_katalog.Where(d => d.id == id).FirstOrDefault();
                data_katalog.deleted_by = "System Check";
                data_katalog.deleted_on = DateTime.Now;

                db.SaveChanges();
                resultMessage.message = "Sukses";
                resultMessage.code = 1;
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;
            }

            return Json(resultMessage);
        }

        public ActionResult edit_katalog(Guid id)
        {
            ViewBag.data_katalog = db.tbl_katalog.Where(d => d.id == id).FirstOrDefault();
            ViewBag.ref_jenis_katalog = db.tbl_ref_jenis_katalog.Where(d => d.deleted_by == null).ToList();

            return View();
        }

        public ActionResult update_katalog(Guid id)
        {
            try
            {
                var data = db.tbl_katalog.Where(d => d.id == id).FirstOrDefault();
                data.name = Request["name"];
                data.harga = Convert.ToInt64(Request["harga"]);
                data.id_jenis_katalog = Guid.Parse(Request["id_jenis_katalog"]);
                data.modified_by = "System Check";
                data.modified_on = DateTime.Now;

                db.SaveChanges();
                resultMessage.message = "Sukses";
                resultMessage.code = 1;
            }
            catch (Exception ex)
            {
                resultMessage.message = ex.ToString();
                resultMessage.code = 0;
            }

            return Json(resultMessage);
        }
    }
}