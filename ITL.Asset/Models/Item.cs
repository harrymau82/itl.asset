﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ITL.Asset.Models
{
    public class tbl_item
    {
        [Key]
        public Guid id { get; set; }
        public string name { get; set; }
        [ForeignKey("tbl_ref_lokasi")]
        public Guid id_lokasi { get; set; }
        [ForeignKey("tbl_katalog")]
        public Guid id_katalog { get; set; }
        public string no_inventaris { get; set; }
        public string kondisi { get; set; }
        public Int32? no_ajuan { get; set; }
        public Int64? harga { get; set; }
        public Int32? garansi { get; set; }
        public DateTime? created_on { get; set; }
        public string created_by { get; set; }
        public DateTime? modified_on { get; set; }
        public string modified_by { get; set; }
        public DateTime? deleted_on { get; set; }
        public string deleted_by { get; set; }
        public virtual tbl_ref_lokasi tbl_ref_lokasi { get; set; }
        public virtual tbl_katalog tbl_katalog { get; set; }
    }

    public class tbl_ref_jenis_katalog
    { 
        [Key]
        public Guid id { get; set; }
        public string name { get; set; }
        public DateTime? created_on { get; set; }
        public string created_by { get; set; }
        public DateTime? modified_on { get; set; }
        public string modified_by { get; set; }
        public DateTime? deleted_on { get; set; }
        public string deleted_by { get; set; }
    }

    public class tbl_katalog
    {
        [Key]
        public Guid id { get; set; }
        public string name { get; set; }
        public Int64? harga { get; set; }
        [ForeignKey("tbl_ref_jenis_katalog")]
        public Guid id_jenis_katalog { get; set; }
        public DateTime? created_on { get; set; }
        public string created_by { get; set; }
        public DateTime? modified_on { get; set; }
        public string modified_by { get; set; }
        public DateTime? deleted_on { get; set; }
        public string deleted_by { get; set; }
        public virtual tbl_ref_jenis_katalog tbl_ref_jenis_katalog { get; set; }
    }

    public class tbl_ref_lokasi
    {
        [Key]
        public Guid id { get; set; }
        public string name { get; set; }
        public string kd_lokasi { get; set; }
        public DateTime? created_on { get; set; }
        public string created_by { get; set; }
        public DateTime? modified_on { get; set; }
        public string modified_by { get; set; }
        public DateTime? deleted_on { get; set; }
        public string deleted_by { get; set; }
    }

    public class SysLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string TableName { get; set; }
        public string RecordID { get; set; }
        public string UserID { get; set; }
        public string ColumnName { get; set; }
        public string CurrentValue { get; set; }
        public string BeforeValue { get; set; }
        public Nullable<DateTime> EventDateUTC { get; set; }
        public string EventType { get; set; }
    }
}